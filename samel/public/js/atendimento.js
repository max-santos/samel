
var div_table_paciente = document.getElementById("table-paciente");
var div_paciente_selecionado = document.getElementById("paciente-selecionado");

var div_table_medico = document.getElementById("table-medico");
var div_medico_selecionado = document.getElementById("medico-selecionado");

var div_paciente_escolhido = document.getElementById("paciente-escolhido");
var div_medico_escolhido = document.getElementById("medico-escolhido");

var lista_paciente = [];
var lista_medico = [];

function pesquisarPaciente() {
    let carregando = "<div class='spinner-border text-info' style='width: 1.5rem; height: 1.5rem;' role='status'>" +
        "<span class='sr-only'>Loading...</span></div>";
    

    div_table_paciente.innerHTML = carregando;
    div_paciente_selecionado.innerHTML = "";
    div_paciente_escolhido.innerHTML = "";

    // let url = 'http://127.0.0.1:8000/api/scnscenario/last/';
    let url = 'search/paciente';

    let json = {
        'search_paciente': $('#search_paciente').val()
    }

    $.ajax({
        method: 'POST',
        data: JSON.stringify(json),
        url: url,
        headers: {'X-CSRF-TOKEN': $('#token-paciente').val()},
        success: function (resultado) {
            console.log('RESULTADO PESQUISA PACIENTE');
            console.log(resultado.data);
            let dados = resultado.data;
            //limpa loading
            div_table_paciente.innerHTML = "";

            let linhas = "";            
            if(dados.length > 0){

                lista_paciente = dados;               

                for(let item of dados){
                    linhas = linhas +`<tr><td><a href="#" onclick="selecionarPaciente(${item.id})" style="width:100%; float: left;">${item.nome}</a></td></tr> `;
                }
                let tabela = `<div class="card mb-3" >
                                <div class="card-body"> 
                                     <div class="table-responsive"> 
                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                            <tbody>  
                                                ${linhas} 
                                             </tbody> 
                                         </table> 
                                     </div> 
                                  </div>
                            </div> `;
                div_table_paciente.innerHTML = tabela;

            }else{                
                div_table_paciente.innerHTML = '<p style="padding-top: 10px;text-align: center;">Nenhum resultado encontrado</p>';
            }
        },
        error: function () {
            // Error occurred loading language file, continue on as best we can
            console.log("ERRO ao PESQUISAR PACIENTES");
            div_table_paciente.innerHTML = "";
            div_table_paciente.innerHTML = '<p style="padding-top: 10px;text-align: center;">Erro ao carregar pacientes</p>';
           
        }
    });
}

function selecionarPaciente(id){
    var result = $.grep(lista_paciente, function(e){ return e.id == id; });
    console.log('PACIENTE SELECIONADO',result[0]);

    div_table_paciente.innerHTML = "";
    div_paciente_selecionado.innerHTML = "";
    div_paciente_escolhido.innerHTML = "";

    let card = `<div class="card mb-3"> 
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6"> 
                                <div class="p-3">                            
                                    <p><b>Nome:</b> ${result[0].nome}</p>
                                    <p><b>Telefone:</b> ${result[0].telefone}</p>
                                    <p><b>Plano de Saúde:</b> ${result[0].plano}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><br>`;
    div_paciente_selecionado.innerHTML = card;
    //seta o id no form do atendimento 
    $('#paciente_id').val(result[0].id);
}

function pesquisarMedico() {
    let carregando = "<div class='spinner-border text-info' style='width: 1.5rem; height: 1.5rem;' role='status'>" +
        "<span class='sr-only'>Loading...</span></div>";
    

    div_table_medico.innerHTML = carregando;
    div_medico_selecionado.innerHTML = "";
    div_medico_escolhido.innerHTML = "";

    // let url = 'http://127.0.0.1:8000/api/scnscenario/last/';
    let url = 'search/medico';

    let json = {
        'search_medico': $('#search_medico').val()
    }

    $.ajax({
        method: 'POST',
        data: JSON.stringify(json),
        url: url,
        headers: {'X-CSRF-TOKEN': $('#token-medico').val()},
        success: function (resultado) {
            console.log('RESULTADO PESQUISA MEDICO');
            console.log(resultado.data);
            let dados = resultado.data;
            //limpa loading
            div_table_medico.innerHTML = "";

            let linhas = "";            
            if(dados.length > 0){

                lista_medico = dados;               

                for(let item of dados){
                    linhas = linhas +`<tr><td><a href="#" onclick="selecionarMedico(${item.id})" style="width:100%; float: left;">${item.nome}</a></td></tr> `;
                }
                let tabela = `<div class="card mb-3" >
                                <div class="card-body"> 
                                     <div class="table-responsive"> 
                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                            <tbody>  
                                                ${linhas} 
                                             </tbody> 
                                         </table> 
                                     </div> 
                                  </div>
                            </div> `;
                div_table_medico.innerHTML = tabela;

            }else{                
                div_table_medico.innerHTML = '<p style="padding-top: 10px;text-align: center;">Nenhum resultado encontrado</p>';
            }
        },
        error: function () {
            // Error occurred loading language file, continue on as best we can
            console.log("ERRO ao PESQUISAR MEDICO");
            div_table_medico.innerHTML = "";
            div_table_medico.innerHTML = '<p style="padding-top: 10px;text-align: center;">Erro ao carregar médicos</p>';
           
        }
    });
}

function selecionarMedico(id){
    var result = $.grep(lista_medico, function(e){ return e.id == id; });
    console.log('MEDICO SELECIONADO',result[0]);

    div_table_medico.innerHTML = "";
    div_medico_selecionado.innerHTML = "";
    div_medico_escolhido.innerHTML = "";

    let card = `<div class="card mb-3"> 
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6"> 
                                <div class="p-3">                            
                                    <p><b>Nome:</b> ${result[0].nome}</p>
                                    <p><b>Especialidade:</b> ${result[0].especialidade}</p>
                                    <p><b>CRM:</b> ${result[0].crm}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><br>`;
    div_medico_selecionado.innerHTML = card;  
    //seta o id no form do atendimento 
    $('#medico_id').val(result[0].id);
}