<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class RelatorioController extends Controller
{   
    
    public function __construct(){
        $this->middleware('auth');
    }
    //view que mostra atendimento por id
    public function show($id){
             
    }
    
    //view de edicao
    public function edit($id){
       
    }
    //exclui registro
    public function destroy($id){
  
    }
    //view de listagem de todos
    public function index(){

       /* $atendimentos = DB::table('atendimentos') 
                        ->join('medicos', 'medicos.id', '=', 'atendimentos.medico_id')
                        ->join('pacientes', 'pacientes.id', '=', 'atendimentos.paciente_id')
                        ->whereNull('atendimentos.deleted_at')
                        ->select('atendimentos.*', 'medicos.nome as medico', 'pacientes.nome as paciente', 'pacientes.plano')
                        ->orderBy('atendimentos.id', 'DESC')->paginate(30); */

        $dados = [];
      
        return view('relatorio.index', compact('dados'));
               
    }
    //view de cadastro
    public function create(){
       
    }
    //cadastra
    public function store(Request $request){ 
       
    }
    //edita 
    public function update(Request $request, $id){        
              
    }
    
    public function delete(Request $request, $id){
      
    }

    public function search(Request $request){ 
        
        //usa isso para validar os campos com os valores inseridos
        $this->validate($request, ['search_inicio'=>'required|string', 
                                   'search_fim'=>'required|string']);
        try {
        $data = $request->all(); 
        
        //tratando data e hora
        $fim= $data["search_fim"]." 23:59:59";
        $inicio = $data["search_inicio"]." 00:00:00";   

        $dados = DB::table('medicos') 
                        ->join('atendimentos', 'atendimentos.medico_id', '=', 'medicos.id')
                        ->join('pacientes', 'pacientes.id', '=', 'atendimentos.paciente_id')
                        ->whereNull('atendimentos.deleted_at')
                        ->where([['atendimentos.created_at', '>=', $inicio], 
                                    ['atendimentos.created_at', '<=', $fim ]])
                        ->select('medicos.nome as medico', 'atendimentos.id as atendimento', 'pacientes.nome as paciente', 'atendimentos.dia as agendamento')
                        ->orderBy('medicos.id', 'DESC')->get();

        return view('relatorio.index', compact('dados'));

        } catch (\Exception $e) {
            if(config('app.debug')){
            return redirect('relatorio/')->with('error', $e->getMessage()); 
            }
            return redirect('relatorio/')->with('error', 'Existe um erro na operação'); 
        }

    }

}
