<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Atendimento;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AtendimentoController extends Controller
{   
    
    private $atendimento;

    public function __construct(Atendimento $atendimento){
        $this->middleware('auth');
        $this->atendimento = $atendimento;
        
    }
    //view que mostra atendimento por id
    public function show($id){
        try {        

            $data = DB::table('atendimentos') 
                        ->join('medicos', 'medicos.id', '=', 'atendimentos.medico_id')
                        ->join('pacientes', 'pacientes.id', '=', 'atendimentos.paciente_id')
                        ->where('atendimentos.id', '=', $id)
                        ->select('atendimentos.*', 'medicos.nome as medico', 'pacientes.nome as paciente', 'pacientes.plano')
                        ->orderBy('atendimentos.id', 'DESC')->get(); 

            $atendimento = $data[0];

            if(!$data || count($data) == 0 || empty($data) || $data == []){               
                return redirect('atendimento/')->with('error', 'Atendimento não encontrado');  
            }
            
            return view('atendimento.view', compact('atendimento'));
        } catch (\Exception $e) {
            if(config('app.debug')){
                return redirect('atendimento/')->with('error', $e->getMessage()); 
            }
            return redirect('atendimento/')->with('error', 'Existe um erro na operação'); 
        }        
    }
    
    //view de edicao
    public function edit($id){

        try {           
            $data = DB::table('atendimentos') 
                        ->join('medicos', 'medicos.id', '=', 'atendimentos.medico_id')
                        ->join('pacientes', 'pacientes.id', '=', 'atendimentos.paciente_id')
                        ->where('atendimentos.id', '=', $id)
                        ->select('atendimentos.*', 
                                'medicos.nome as medico', 'medicos.especialidade', 'medicos.crm',
                                'pacientes.nome as paciente', 'pacientes.plano', 'pacientes.telefone')
                        ->get(); 

            $atendimento = $data[0];
            //tratando data ehora para serem editados em inputs diferentes
            $dataAtendimento = explode(" ", $atendimento->dia);
            $atendimento->dia = $dataAtendimento[0];

            //tratando hora que possue apenas minuto e segundo no input
            $horaAtendimento = explode(":", $dataAtendimento[1]);
            $atendimento->hora = $horaAtendimento[0].":".$horaAtendimento[1];


            if(!$data || count($data) == 0 || empty($data) || $data == []){               
                return redirect('atendimento/'.$id.'/edit')->with('error', 'Atendimento não encontrado');  
            }

            return view('atendimento.edit', compact('atendimento'));

        } catch (\Exception $e) {
            if(config('app.debug')){
                return redirect('atendimento/'.$id.'/edit')->with('error', $e->getMessage()); 
            }
            return redirect('atendimento/'.$id.'/edit')->with('error', 'Existe um erro na operação'); 
        }        
    }
    //exclui registro
    public function destroy($id){

        try {
            $atendimento = $this->atendimento->findOrFail($id); 
            $atendimento->delete(); 
            
            return redirect('atendimento/')->with('success', 'Atendimento excluído com sucesso'); 

        } catch (\Exception $e) {
            if(config('app.debug')){
                return redirect('atendimento/')->with('error', $e->getMessage()); 
            }
            return redirect('atendimento/')->with('error', 'Existe um erro na operação'); 
        }   
    }
    //view de listagem de todos
    public function index(){

        $atendimentos = DB::table('atendimentos') 
                        ->join('medicos', 'medicos.id', '=', 'atendimentos.medico_id')
                        ->join('pacientes', 'pacientes.id', '=', 'atendimentos.paciente_id')
                        ->whereNull('atendimentos.deleted_at')
                        ->select('atendimentos.*', 'medicos.nome as medico', 'pacientes.nome as paciente', 'pacientes.plano')
                        ->orderBy('atendimentos.id', 'DESC')->paginate(30); 

      
        return view('atendimento.index', compact('atendimentos'));
               
    }
    //view de cadastro
    public function create(){
        return view('atendimento.create');
    }
    //cadastra
    public function store(Request $request){ 

            //usa isso para validar os campos com os valores inseridos
            $this->validate($request, ['medico_id'=>'required|string', 
                                       'paciente_id'=>'required|string',
                                       'dia' => 'required|string',
                                       'hora' => 'required|string']);
            try {
            $data = $request->all();  
            $data['dia'] = $data['dia']." ".$data['hora'].":00";            

            $atendimento = $this->atendimento->create($data);

            return redirect('atendimento/')->with('success', 'Atendimento criado com sucesso'); 
            
        } catch (\Exception $e) {
            if(config('app.debug')){
                return redirect('atendimento/')->with('error', $e->getMessage()); 
            }
            return redirect('atendimento/')->with('error', 'Existe um erro na operação'); 
        }         
    }
    //edita 
    public function update(Request $request, $id){        
       
        $this->validate($request, ['medico_id'=>'required|string', 
                                    'paciente_id'=>'required|string',
                                    'dia' => 'required|string',
                                    'hora' => 'required|string']);
        try {
            $data = $request->all();  
            $data['dia'] = $data['dia']." ".$data['hora'].":00";  
            $atendimento = $this->atendimento->findOrFail($id);
            $atendimento->update($data);

            return redirect('atendimento/'.$id.'/edit')->with('success', 'Atendimento editado com sucesso');                     

        } catch (\Exception $e) {
            if(config('app.debug')){
                return redirect('atendimento/'.$id.'/edit')->with('error', $e->getMessage()); 
            }
            return redirect('atendimento/'.$id.'/edit')->with('error', 'Existe um erro na operação'); 
        }          
    }
    
    //desativa o registro apenas usando o campo deleted_at, nao exclui do banco 
    //(verificar se existe esse campo no banco)
    public function delete(Request $request, $id){

        $data = $request->all();
        try {      

            $affected = DB::table('atendimentos')
              ->where('id', $id)
              ->update(['deleted_at' => $data['deleted_at']]);
            
            return redirect('atendimento/')->with('success', "Atendimento desativado com sucesso");                      

        } catch (\Exception $e) {
            if(config('app.debug')){
                return redirect('atendimento/')->with('error', $e->getMessage()); 
            }
            return redirect('atendimento/')->with('error', 'Existe um erro na operação'); 
        }          
    }

}