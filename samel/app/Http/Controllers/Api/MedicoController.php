<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class MedicoController extends Controller
{
     //INTEGRAÇÃO API 
     public function getCRM(Request $request){
        try {
            $data = json_decode($request->getContent());
            if($data->crm != ""){
                
                //procura dados do medico
                $dadosmedico = DB::table('medicos')->where('medicos.crm', '=', $data->crm)->get();
                
                if(!$dadosmedico || count($dadosmedico) == 0 || empty($dadosmedico) || $dadosmedico == []){               
                    return response()->json(['erro'=>'Médico não foi encontrado'], 404); 
                }
                $medico = $dadosmedico[0];

                //procura atendimento do medico encontrado
                $atendimentos = DB::table('atendimentos') 
                ->join('pacientes', 'pacientes.id', '=', 'atendimentos.paciente_id')
                ->whereNull('atendimentos.deleted_at')
                ->where('atendimentos.medico_id', '=', $medico->id)
                ->select('atendimentos.id', 'atendimentos.dia', 
                          'pacientes.nome as paciente', 'pacientes.telefone', 'pacientes.plano')
                ->orderBy('atendimentos.id', 'DESC')->get(); 

                $json = ['medico'=>$medico, 'atendimentos'=> $atendimentos];
                
                return response()->json($json);
            }else{
                return response()->json('CRM inválido', 404);
            }           

        } catch (\Exception $e) {
            if(config('app.debug')){
                return response()->json($e->getMessage(), 404);
            }
            return response()->json('Existe um erro na operação', 404);
        }           
    }
}
