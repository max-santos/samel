<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paciente;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PacienteController extends Controller
{
   
    
    private $paciente;

    public function __construct(Paciente $paciente){

        $this->paciente = $paciente;
        $this->middleware('auth');
    }
    //view que mostra paciente por id
    public function show($id){
        try {
            $data = DB::table('pacientes') 
                            ->where([['pacientes.id', '=', $id]])
                            ->get();         
            if(!$data || count($data) == 0 || empty($data) || $data == []){               
                return redirect('paciente/')->with('error', 'Paciente não encontrado');  
            }
            $paciente = $data[0];
            
            return view('paciente.view', compact('paciente'));
        } catch (\Exception $e) {
            if(config('app.debug')){
                return redirect('paciente/')->with('error', $e->getMessage()); 
            }
            return redirect('paciente/')->with('error', 'Existe um erro na operação'); 
        }

        
    }
    //view de edicao
    public function edit($id){

        try {
            $paciente = $this->paciente->findOrFail($id);          
            return view('paciente.edit', compact('paciente'));

        } catch (\Exception $e) {
            if(config('app.debug')){
                return redirect('paciente/')->with('error', $e->getMessage()); 
            }
            return redirect('paciente/')->with('error', 'Existe um erro na operação'); 
        }        
    }
    //exclui registro
    public function destroy($id){

        try {
            $paciente = $this->paciente->findOrFail($id); 
            $paciente->delete(); 
            
            return redirect('paciente/')->with('success', 'Paciente excluído com sucesso'); 

        } catch (\Exception $e) {
            if(config('app.debug')){
                return redirect('paciente/')->with('error', $e->getMessage()); 
            }
            return redirect('paciente/')->with('error', 'Existe um erro na operação'); 
        }   
    }
    //view de listagem de todos
    public function index(){
      
            $pacientes = $this->paciente->whereNull('deleted_at')
                                ->orderBy('id', 'DESC')->paginate(30);
            return view('paciente.index', compact('pacientes'));
               
    }
    //view de cadastro
    public function create(){
        return view('paciente.create');
    }
    //cadastra
    public function store(Request $request){ 

            //usa isso para validar os campos com os valores inseridos
            $this->validate($request, ['nome'=>'required|string|max:300|regex:/^[a-zA-Z\s]*$/', 
                                       'telefone'=>'required|string|max:14',
                                       'plano' => 'required |string | max:100']);
            try {
            $data = $request->all();    
            $paciente = $this->paciente->create($data);

            return redirect('paciente/')->with('success', 'Paciente criado com sucesso'); 
            
        } catch (\Exception $e) {
            if(config('app.debug')){
                return redirect('paciente/')->with('error', $e->getMessage()); 
            }
            return redirect('paciente/')->with('error', 'Existe um erro na operação'); 
        }         
    }
    //edita 
    public function update(Request $request, $id){        
       
        $this->validate($request, ['nome'=>'required|string|max:300|regex:/^[a-zA-Z\s]*$/', 
                                    'telefone'=>'required|string|max:14',
                                    'plano' => 'required |string | max:100']);
        try {
            $data = $request->all();  
            $paciente = $this->paciente->findOrFail($id);
            $paciente->update($data);

            return redirect('paciente/'.$id.'/edit')->with('success', 'Paciente editado com sucesso');                     

        } catch (\Exception $e) {
            if(config('app.debug')){
                return redirect('paciente/'.$id.'/edit')->with('error', $e->getMessage()); 
            }
            return redirect('paciente/'.$id.'/edit')->with('error', 'Existe um erro na operação'); 
        }          
    }
    
    //desativa o registro apenas usando o campo deleted_at, nao exclui do banco 
    //(verificar se existe esse campo no banco)
    public function delete(Request $request, $id){

        $data = $request->all();
        try {      

            $affected = DB::table('pacientes')
              ->where('id', $id)
              ->update(['deleted_at' => $data['deleted_at']]);
            
            return redirect('paciente/')->with('success', "Paciente desativado com sucesso");                      

        } catch (\Exception $e) {
            if(config('app.debug')){
                return redirect('paciente/')->with('error', $e->getMessage()); 
            }
            return redirect('paciente/')->with('error', 'Existe um erro na operação'); 
        }          
    }

    //view de pesquisa de paciente por nome
    public function search(Request $request){
        try {
            $data = json_decode($request->getContent());
            if($data->search_paciente != ""){
                $pacientes = $this->paciente->where('nome', 'ilike', '%'.$data->search_paciente.'%')
                ->orderBy('id', 'DESC')->paginate(30);

                return response()->json($pacientes);
            }else{
                return response()->json([], 404);
            }           

        } catch (\Exception $e) {
            if(config('app.debug')){
                return response()->json($e->getMessage(), 404);
            }
            return response()->json('Existe um erro na operação', 404);
        }           
    }

}
