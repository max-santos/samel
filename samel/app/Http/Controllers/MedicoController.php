<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Medico;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MedicoController extends Controller
{
   
    
        private $medico;
    
        public function __construct(Medico $medico){

            $this->medico = $medico;
            $this->middleware('auth');
        }
        //view que mostra medico por id
        public function show($id){
            try {
                $data = DB::table('medicos') 
                                ->where([['medicos.id', '=', $id]])
                                ->get();         
                if(!$data || count($data) == 0 || empty($data) || $data == []){               
                    return redirect('medico/')->with('error', 'Médico não encontrado');  
                }
                $medico = $data[0];
                
                return view('medico.view', compact('medico'));
            } catch (\Exception $e) {
                if(config('app.debug')){
                    return redirect('medico/')->with('error', $e->getMessage()); 
                }
                return redirect('medico/')->with('error', 'Existe um erro na operação'); 
            }
    
            
        }
        //view de edicao
        public function edit($id){
    
            try {
                $medico = $this->medico->findOrFail($id);          
                return view('medico.edit', compact('medico'));
    
            } catch (\Exception $e) {
                if(config('app.debug')){
                    return redirect('medico/')->with('error', $e->getMessage()); 
                }
                return redirect('medico/')->with('error', 'Existe um erro na operação'); 
            }        
        }
        //exclui registro
        public function destroy($id){
    
            try {
                $medico = $this->medico->findOrFail($id); 
                $medico->delete(); 
                
                return redirect('medico/')->with('success', 'Médico excluído com sucesso'); 
    
            } catch (\Exception $e) {
                if(config('app.debug')){
                    return redirect('medico/')->with('error', $e->getMessage()); 
                }
                return redirect('medico/')->with('error', 'Existe um erro na operação'); 
            }   
        }
        //view de listagem de todos
        public function index(){
          
                $medicos = $this->medico->whereNull('deleted_at')
                                    ->orderBy('id', 'DESC')->paginate(30);
                return view('medico.index', compact('medicos'));
                   
        }
        //view de cadastro
        public function create(){
            return view('medico.create');
        }
        //cadastra
        public function store(Request $request){
    
                //usa isso para validar os campos com os valores inseridos
                $this->validate($request, ['nome'=>'required|string|max:300|regex:/^[a-zA-Z\s]*$/', 
                                            'especialidade'=>'required|string|max:255',
                                            'crm' => 'required |string | max:30']);
                try {
                $data = $request->all();    
                $medico = $this->medico->create($data);
    
                return redirect('medico/')->with('success', 'Médico criado com sucesso'); 
                
            } catch (\Exception $e) {
                if(config('app.debug')){
                    return redirect('medico/')->with('error', $e->getMessage()); 
                }
                return redirect('medico/')->with('error', 'Existe um erro na operação'); 
            }         
        }
        //edita 
        public function update(Request $request, $id){        
           
            $this->validate($request, ['nome'=>'required|string|max:300|regex:/^[a-zA-Z\s]*$/', 
                                           'especialidade'=>'required|string|max:255',
                                           'crm' => 'required |string | max:30']);
            try {
                $data = $request->all();  
                $medico = $this->medico->findOrFail($id);
                $medico->update($data);
    
                return redirect('medico/'.$id.'/edit')->with('success', 'Médico editado com sucesso');                     
    
            } catch (\Exception $e) {
                if(config('app.debug')){
                    return redirect('medico/'.$id.'/edit')->with('error', $e->getMessage()); 
                }
                return redirect('medico/'.$id.'/edit')->with('error', 'Existe um erro na operação'); 
            }          
        }
        
        //desativa o registro apenas usando o campo deleted_at, nao exclui do banco 
        //(verificar se existe esse campo no banco)
        public function delete(Request $request, $id){
    
            $data = $request->all();
            try {      
    
                $affected = DB::table('medicos')
                  ->where('id', $id)
                  ->update(['deleted_at' => $data['deleted_at']]);
                
                return redirect('medico/')->with('success', "Médico desativado com sucesso");                      
    
            } catch (\Exception $e) {
                if(config('app.debug')){
                    return redirect('medico/')->with('error', $e->getMessage()); 
                }
                return redirect('medico/')->with('error', 'Existe um erro na operação'); 
            }          
        }

        //view de pesquisa de medico por nome
        public function search(Request $request){
            try {
                $data = json_decode($request->getContent());
                if($data->search_medico != ""){
                    $medicos = $this->medico->where('nome', 'ilike', '%'.$data->search_medico.'%')
                    ->orderBy('id', 'DESC')->paginate(30);

                    return response()->json($medicos);
                }else{
                    return response()->json([], 404);
                }           

            } catch (\Exception $e) {
                if(config('app.debug')){
                    return response()->json($e->getMessage(), 404);
                }
                return response()->json('Existe um erro na operação', 404);
            }           
        }

       
    
}
