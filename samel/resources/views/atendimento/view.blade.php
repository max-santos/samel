@extends('layouts.principal')

@section('content')
        <div class="d-sm-flex align-items-center justify-content-between ">
            <h1 class="h3 mb-0 text-gray-800">Visualizar Atendimento</h1>
        </div>
        <!-- Outer Row -->
        <div class="row justify-content-center">
            <div class="col-xl-12 col-lg-12 col-md-9">

            <a href="{{route('atendimento.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-1">
                <i class="fas fa-arrow-left"></i> Voltar</a><br>
                <div class="card o-hidden border-0 shadow-lg my-2">
                    
                    <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6"> 
                            <div class="p-3">                            
                                <p><b>Paciente:</b> {{$atendimento->paciente}}</p>
                                <p><b>Médico:</b> {{$atendimento->medico}}</p>
                                <p><b>Plano de Saúde:</b> {{$atendimento->plano}}</p>
                                <p><b>Data de Agendamento:</b> {{$atendimento->dia}}</p>
                                <p><b>Última atualização:</b> {{$atendimento->updated_at}}</p>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>

@endsection