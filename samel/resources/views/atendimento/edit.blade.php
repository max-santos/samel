@extends('layouts.principal')

@section('content')
        <div class="d-sm-flex align-items-center justify-content-between ">
            <h1 class="h3 mb-0 text-gray-800">Editar Atendimento</h1>
            <!--a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a-->
        </div>
        <!-- Outer Row -->
        <div class="row justify-content-center">
            <div class="col-xl-12 col-lg-12 col-md-9">
                <a href="{{route('atendimento.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-1">
                    <i class="fas fa-arrow-left"></i> Voltar</a><br>
                <div class="card o-hidden border-0 shadow-lg my-2">
                    <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6"> 
                            <div class="p-3">
                                <!--Nome FORM-->
                                <div class="form-group">
                                    <p class="text-left text-primary p-0">{{ __('Paciente:') }}</p>
                                    <div class="input-group">
                                        <input type="text" name="search_paciente" id="search_paciente" class="form-control small" placeholder="Procurar paciente" aria-label="Search" aria-describedby="basic-addon2">
                                        <input type="hidden" name="_token" id="token-paciente" value="{{ csrf_token() }}">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="pesquisarPaciente()">
                                            <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>                                        
                                </div>
                                <!--END Nome FORM-->
                                <!--RESULTADO DA PESQUISA DO PACIENTE-->
                                <div id="table-paciente">
                                
                                </div>
                                <!--FIM RESULTADO DA PESQUISA DO PACIENTE-->

                                <!--RESULTADO ESCOLHIDO DA PESQUISA DO PACIENTE-->
                                <div id="paciente-selecionado">
                                
                                </div>  
                                @if(isset($atendimento))
                                    <div id="paciente-escolhido">
                                        <div class="card mb-3"> 
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-6"> 
                                                    <div class="p-3">                            
                                                        <p><b>Nome:</b> {{$atendimento->paciente}}</p>
                                                        <p><b>Telefone:</b> {{$atendimento->telefone}}</p>
                                                        <p><b>Plano de Saúde:</b> {{$atendimento->plano}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><br>
                                    </div>
                                @endif                        
                                <!--FIM RESULTADO ESCOLHIDO DA PESQUISA DO PACIENTE-->
                                
                                <!--Médico FORM-->    
                                <div class="form-group">
                                    <p class="text-left text-primary p-0">{{ __('Médico:') }}</p>
                                    <div class="input-group">
                                        <input type="text" name="search_medico" id="search_medico" class="form-control small" placeholder="Procurar médico" aria-label="Search" aria-describedby="basic-addon2">
                                        <input type="hidden" name="_token" id="token-medico" value="{{ csrf_token() }}">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="pesquisarMedico()">
                                            <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>                                        
                                </div>
                                <!--END Médico FORM--> 

                                <!--RESULTADO DA PESQUISA DO Médico-->
                                <div id="table-medico">
                                
                                </div>
                                <!--FIM RESULTADO DA PESQUISA DO Médico-->

                                <!--RESULTADO ESCOLHIDO DA PESQUISA DO Médico-->
                                <div id="medico-selecionado">
                                
                                </div>    
                                @if(isset($atendimento))
                                    <div id="medico-escolhido">
                                        <div class="card mb-3"> 
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-6"> 
                                                        <div class="p-3">                            
                                                            <p><b>Nome:</b> {{$atendimento->medico}}</p>
                                                            <p><b>Especialidade:</b> {{$atendimento->especialidade}}</p>
                                                            <p><b>CRM:</b> {{$atendimento->crm}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><br> 
                                    </div>    
                                @endif                      
                                <!--FIM RESULTADO ESCOLHIDO DA PESQUISA DO Médico-->

                                @include('utils.message')
                                <form class="user" method="POST" action="{{ route('atendimento.update', $atendimento->id) }}">  
                                    @csrf
                                    @method("PUT")
                                    <!--Dia FORM-->                            
                                    <div class="form-group">                                        

                                        @error('paciente_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                        <div class="form-row">
                                            <div class="col">
                                                <p class="text-left text-primary p-0">{{ __('Dia:') }}</p>
                                                <input type="date" name="dia" class="form-control @error('dia') is-invalid @enderror" id="dia" aria-describedby="diaHelp" value="{{$atendimento->dia}}"  autocomplete="dia" autofocus>
                                                @error('dia')
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>Por favor, informe uma Data</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col">
                                                <p class="text-left text-primary p-0">{{ __('Hora:') }}</p>
                                                <input type="time" name="hora" class="form-control @error('hora') is-invalid @enderror" id="hora" aria-describedby="horaHelp"  value="{{$atendimento->hora}}"  autocomplete="hora" autofocus>
                                                @error('hora')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Por favor, informe uma Hora</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <!--END Dia FORM-->    
                                    <input type="hidden" class="form-control @error('medico_id') is-invalid @enderror" name="medico_id"  value="{{$atendimento->medico_id}}" id="medico_id">
                                    @error('medico_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>Por favor, selecione um Médico</strong>
                                        </span>
                                    @enderror
                                    <input type="hidden" class="form-control @error('paciente_id') is-invalid @enderror" name="paciente_id" value="{{$atendimento->paciente_id}}" id="paciente_id"> 
                                    @error('paciente_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>Por favor, selecione um Paciente</strong>
                                        </span>
                                    @enderror                          
                                   
                                    <div class="form-group row">
                                        <div class="col-md-4 mb-1 offset-md-2">
                                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                                Salvar
                                            </button>
                                        </div>    
                                        <div class="col-md-4">
                                            <a  class="btn btn-primary btn-user btn-block" style="color: #fff;" href="{{ route('atendimento.index') }}">
                                                Cancelar
                                            </a>
                                        </div>                         
                                    </div>  
                                </form>                                  
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    @push('js-funcoes')
    <script src="{{ asset('js/atendimento.js') }}"></script>
    @endpush

@endsection