@extends('layouts.principal')

@section('content')

    <a href="{{route('atendimento.create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-1">
    <i class="fas fa-plus fa-sm text-white-50"></i> Adicionar Atendimento</a><br> 

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Atendimento Ativos</h6>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                @include('utils.message')
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>Cód.</th>
                    <th>Paciente</th>
                    <th>Médico</th>
                    <th>Plano de Saúde</th>
                    <th>Dia</th>
                    <th>Operações</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Cód.</th>
                    <th>Paciente</th>
                    <th>Médico</th>
                    <th>Plano de Saúde</th>
                    <th>Dia</th>
                    <th>Operações</th>
                </tr>
                </tfoot>
                <tbody> 
                @forelse($atendimentos as $value)
                    <tr>
                        <td>SML-{{$value->id}}</td>
                        <td>{{$value->paciente}}</td>
                        <td>{{$value->medico}}</td>
                        <td>{{$value->plano}}</td>
                        <td>{{$value->dia}}</td>
                        <td>
                            
                            <a class="btn btn-info btn-sm" href="{{route('atendimento.show', $value->id)}}" alt="Mostrar atendimento" title="Mostrar atendimento">
                                <i class="fas fa-eye"></i></a>
                                <a class="btn btn-success btn-sm" href="{{route('atendimento.edit', $value->id)}}" alt="Editar atendimento" title="Editar atendimento">
                                <i class="fas fa-user-edit"></i></a>  
                                              
                                <a class="btn btn-danger btn-sm" href="#" alt="Excluir atendimento" title="Excluir atendimento" 
                                    data-toggle="modal" data-target="#inactiveModal{{$value->id}}">
                                    <i class="fas fa-lock"></i></a>                                    
                          
                                <!-- Delete Modal-->
                                <div class="modal fade" id="inactiveModal{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Deseja Excluir Atendimento?</h5>
                                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">Selecione "Excluir" para concluir a operação</div>
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>                                   
                                                <form action="{{ url('/atendimento', ['id' => $value->id]) }}" method="post">
                                                    <input class="btn btn-primary" type="submit" value="Excluir" />
                                                    @method('delete')
                                                    @csrf
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </td>
                    </tr> 
                @empty
                <tr>
                        <td colspan="6">
                            <h2>Dados Não Encontrados</h2>
                        </td>
                </tr> 

                
                @endforelse
                </tbody>
                {{$atendimentos->links()}}
            </table>
            </div>
        </div>
    </div>

@endsection
