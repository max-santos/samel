@extends('layouts.principal')

@section('content')
        <div class="d-sm-flex align-items-center justify-content-between ">
            <h1 class="h3 mb-0 text-gray-800">Editar Paciente</h1>
            <!--a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a-->
        </div>
        <!-- Outer Row -->
        <div class="row justify-content-center">
            <div class="col-xl-12 col-lg-12 col-md-9">
                <a href="{{route('paciente.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-1">
                    <i class="fas fa-arrow-left"></i> Voltar</a><br>
                <div class="card o-hidden border-0 shadow-lg my-2">
                    <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6"> 
                            <div class="p-3">

                                @include('utils.message')
                        
                                <form class="user" method="POST" action="{{ route('paciente.update', $paciente->id) }}">  
                                    @csrf
                                    @method("PUT")
                                    <!--nome FORM-->
                                    <div class="form-group">
                                        <p class="text-left text-primary p-0">{{ __('Nome:') }}</p>
                                        <input type="text" name="nome" class="form-control @error('nome') is-invalid @enderror" value="{{ $paciente->nome }}" id="nome" aria-describedby="nomeHelp" autofocus placeholder="Ex: Maria">
                                        @error('nome')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div> 
                                    <!--END nome FORM-->  

                                    <!--Telefone FORM-->
                                    <div class="form-group">
                                        <p class="text-left text-primary p-0">{{ __('Telefone:') }}</p>
                                        <input type="text" name="telefone" class="form-control @error('telefone') is-invalid @enderror" value="{{ $paciente->telefone }}" id="telefone" aria-describedby="telefoneHelp" autofocus placeholder="Ex: 92 992999999">
                                        @error('telefone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div> 
                                    <!--END Telefone FORM--> 

                                    <!--Plano FORM-->
                                    <div class="form-group">
                                        <p class="text-left text-primary p-0">{{ __('Plano de Saúde:') }}</p>
                                        <select class="form-control" name="plano" id="plano">
                                            <option value="PME" 
                                                @if ($paciente->plano == 'PME')
                                                    selected="selected"
                                                @endif >PME</option>
                                            <option value="Empresarial"
                                                @if ($paciente->plano == 'Empresarial')
                                                    selected="selected"
                                                @endif>Empresarial</option>
                                        </select>
                                    </div>
                                    <!--END Plano FORM-->                                    
                                    
                                    <div class="form-group row">
                                        <div class="col-md-4 mb-1 offset-md-2">
                                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                                Salvar
                                            </button>
                                        </div>    
                                        <div class="col-md-4">
                                            <a  class="btn btn-primary btn-user btn-block" style="color: #fff;" href="{{ route('paciente.index') }}">
                                                Cancelar
                                            </a>
                                        </div>                         
                                    </div>  

                                </form>                                    
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>

@endsection