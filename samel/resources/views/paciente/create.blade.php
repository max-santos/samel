@extends('layouts.principal')

@section('content')
        <div class="d-sm-flex align-items-center justify-content-between ">
            <h1 class="h3 mb-0 text-gray-800">Adicionar Paciente</h1>          
        </div>
        <!-- Outer Row -->
        <div class="row justify-content-center">
            <div class="col-xl-12 col-lg-12 col-md-9">
                <div class="card o-hidden border-0 shadow-lg my-2">
                    <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6"> 
                            <div class="p-3">
                                
                                <form class="user" method="post" action="{{ route('paciente.store') }}">  
                                    @csrf
                                    <!--Nome FORM-->
                                    <div class="form-group">
                                        <p class="text-left text-primary p-0">{{ __('Nome:') }}</p>
                                        <input type="text" name="nome" class="form-control @error('nome') is-invalid @enderror" value="{{ old('nome') }}" id="nome" aria-describedby="nomeHelp"  autofocus placeholder="Ex: Maria">
                                        @error('nome')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div> 
                                    <!--END Nome FORM-->

                                    <!--Telefone FORM-->                            
                                    <div class="form-group">
                                        <p class="text-left text-primary p-0">{{ __('Telefone:') }}</p>
                                        <input type="text" name="telefone" class="form-control @error('telefone') is-invalid @enderror" id="telefone" aria-describedby="telefoneHelp" value="{{ old('telefone') }}"  autocomplete="telefone" autofocus placeholder="Ex: 92 992999999">
                                        @error('telefone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <!--END Telefone FORM-->

                                    <!--Plano FORM-->  
                                    <div class="form-group">
                                        <p class="text-left text-primary p-0">{{ __('Plano de Saúde:') }}</p>
                                        <select class="form-control" name="plano" id="plano">
                                            <option value="PME">PME</option>
                                            <option value="Empresarial">Empresarial</option>
                                        </select>
                                    </div>
                                     <!--Plano FORM-->                                     
                                   
                                    <div class="form-group row">
                                        <div class="col-md-4 mb-1 offset-md-2">
                                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                                Salvar
                                            </button>
                                        </div>    
                                        <div class="col-md-4">
                                            <a  class="btn btn-primary btn-user btn-block" style="color: #fff;" href="{{ route('paciente.index') }}">
                                                Cancelar
                                            </a>
                                        </div>                         
                                    </div>  

                                </form>                                    
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>

@endsection