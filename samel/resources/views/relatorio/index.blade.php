@extends('layouts.principal')

@section('content')

    <?php
        
        $data_inicio = date('Y-m-d');  
        $data_fim = date('Y-m-d');        
    ?>

    <div class="d-sm-flex align-items-center justify-content-between ">
            <h1 class="h3 mb-2 text-gray-800">Relatório de Atendimentos</h1>
        </div>
        <a href="{{route('home')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-1">
                    <i class="fas fa-arrow-left"></i> Voltar</a><br>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <form class="d-none d-sm-inline-block mt-3 form-inline ml-md-3 my-2  mw-100 navbar-search" 
            method="POST" action="{{route('search.relatorio')}}">
            @csrf
            <div class="col-lg-7">
                <div class="input-group">
                        <label for="search_inicio" class="ml-3 mr-1">Data Inicial:</label>
                        <input type="date" class="form-control border-1 small mr-3  @error('search_inicio') is-invalid @enderror" 
                                name="search_inicio" id="search_inicio" value="{{ $data_inicio }}" autofocus>
                        <label for="search_fim" class="mr-1">Data Final:</label>
                        <input type="date" class="form-control border-1 small @error('search_fim') is-invalid @enderror" 
                                name="search_fim" id="search_fim" value="{{ $data_fim }}" autofocus>     
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit" >
                            <i class="fas fa-search fa-sm"></i>
                            </button>
                        </div>      
                </div>
            </div>

            @error('search_inicio')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            @error('search_fim')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </form>
        <div class="card-body">
            <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>                        
                        <th>Médico</th>
                        <th>Atendimento</th>
                        <th>Paciente</th>
                        <th>Data de Agendamento</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Médico</th>
                        <th>Atendimento</th>
                        <th>Paciente</th>
                        <th>Data de Agendamento</th>
                    </tr>
                </tfoot>
                <tbody> 
                @forelse($dados as $value)
                    <tr>
                        <th>{{$value->medico}}</th>
                        <th>SML-{{$value->atendimento}}</th>
                        <th>{{$value->paciente}}</th>
                        <th>{{$value->agendamento}}</th>                   
                    </tr> 
                @empty
                    <tr>
                        <td colspan="4">
                            <h2>Atendimentos não Encontrados</h2>
                        </td>
                    </tr> 
                @endforelse
                </tbody>
            </table>
            </div>
        </div>
    </div>


@endsection

<script>
    function mudarParaExportar(valor){        
        console.log("Aqui");
        let vl_input = document.querySelector("#download");
        vl_input.value = valor;
        console.log("Mudou");
    }
</script>
