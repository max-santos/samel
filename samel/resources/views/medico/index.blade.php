@extends('layouts.principal')

@section('content')

    <a href="{{route('medico.create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-1">
    <i class="fas fa-plus fa-sm text-white-50"></i> Adicionar Médico</a><br> 

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Médicos Ativos</h6>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                @include('utils.message')
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Especialidade</th>
                    <th>CRM</th>
                    <th>Data de Criação</th>
                    <th>Operações</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Especialidade</th>
                    <th>CRM</th>
                    <th>Data de Criação</th>
                    <th>Operações</th>
                </tr>
                </tfoot>
                <tbody> 
                @forelse($medicos as $value)
                    <tr>
                        <td>{{$value->id}}</td>
                        <td>{{$value->nome}}</td>
                        <td>{{$value->especialidade}}</td>
                        <td>{{$value->crm}}</td>
                        <td>{{$value->created_at}}</td>
                        <td>
                            
                            <a class="btn btn-info btn-sm" href="{{route('medico.show', $value->id)}}" alt="Mostrar médico" title="Mostrar médico">
                                <i class="fas fa-eye"></i></a>
                                <a class="btn btn-success btn-sm" href="{{route('medico.edit', $value->id)}}" alt="Editar médico" title="Editar médico">
                                <i class="fas fa-user-edit"></i></a>  
                                              
                                <a class="btn btn-danger btn-sm" href="#" alt="Excluir médico" title="Excluir médico" 
                                    data-toggle="modal" data-target="#inactiveModal{{$value->id}}">
                                    <i class="fas fa-lock"></i></a>                                    
                          
                                <!-- Delete Modal-->
                                <div class="modal fade" id="inactiveModal{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Deseja Excluir Médico?</h5>
                                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">Selecione "Excluir" para concluir a operação</div>
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>                                   
                                                <form action="{{ url('/medico', ['id' => $value->id]) }}" method="post">
                                                    <input class="btn btn-primary" type="submit" value="Excluir" />
                                                    @method('delete')
                                                    @csrf
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </td>
                    </tr> 
                @empty
                <tr>
                        <td colspan="6">
                            <h2>Dados Não Encontrados</h2>
                        </td>
                </tr> 

                
                @endforelse
                </tbody>
                {{$medicos->links()}}
            </table>
            </div>
        </div>
    </div>

@endsection
