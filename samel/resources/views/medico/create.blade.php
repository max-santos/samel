@extends('layouts.principal')

@section('content')
        <div class="d-sm-flex align-items-center justify-content-between ">
            <h1 class="h3 mb-0 text-gray-800">Adicionar Médico</h1>          
        </div>
        <!-- Outer Row -->
        <div class="row justify-content-center">
            <div class="col-xl-12 col-lg-12 col-md-9">
                <div class="card o-hidden border-0 shadow-lg my-2">
                    <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6"> 
                            <div class="p-3">
                                
                                <form class="user" method="post" action="{{ route('medico.store') }}">  
                                    @csrf
                                    <!--Nome FORM-->
                                    <div class="form-group">
                                        <p class="text-left text-primary p-0">{{ __('Nome:') }}</p>
                                        <input type="text" name="nome" class="form-control @error('nome') is-invalid @enderror" value="{{ old('nome') }}" id="nome" aria-describedby="nomeHelp"  autofocus placeholder="Ex: Dr. Thomas">
                                        @error('nome')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div> 
                                    <!--END Nome FORM-->

                                    <!--Especialidade FORM-->  
                                    <div class="form-group">
                                        <p class="text-left text-primary p-0">{{ __('Especialidade:') }}</p>
                                        <select class="form-control" name="especialidade" id="especialidade">
                                            <option value="Pediatra">Pediatra</option>
                                            <option value="Clínico Geral">Clínico Geral</option>
                                            <option value="Medicina do Trabalho">Medicina do Trabalho</option>
                                        </select>
                                    </div>
                                     <!--Especialidade FORM--> 

                                    <!--crm FORM-->                            
                                    <div class="form-group">
                                        <p class="text-left text-primary p-0">{{ __('CRM:') }}</p>
                                        <input type="text" name="crm" class="form-control @error('crm') is-invalid @enderror" id="crm" aria-describedby="crmHelp" value="{{ old('crm') }}"  autocomplete="crm" autofocus placeholder="Ex: 5265">
                                        @error('crm')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <!--END crm FORM-->
                                   
                                    <div class="form-group row">
                                        <div class="col-md-4 mb-1 offset-md-2">
                                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                                Salvar
                                            </button>
                                        </div>    
                                        <div class="col-md-4">
                                            <a  class="btn btn-primary btn-user btn-block" style="color: #fff;" href="{{ route('medico.index') }}">
                                                Cancelar
                                            </a>
                                        </div>                         
                                    </div>  

                                </form>                                    
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>

@endsection