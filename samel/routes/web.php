<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('medico', 'MedicoController');
Route::resource('paciente', 'PacienteController');
Route::resource('atendimento', 'AtendimentoController');
Route::resource('relatorio', 'RelatorioController');

//pesquisas na tela de atendimento
Route::post('atendimento/search/medico', 'MedicoController@search')->name('search.medico');
Route::post('atendimento/search/paciente', 'PacienteController@search')->name('search.paciente');

Route::post('atendimento/{id}/search/medico', 'MedicoController@search')->name('search.medico');
Route::post('atendimento/{id}/search/paciente', 'PacienteController@search')->name('search.paciente');

//pesquisa do relatorio
Route::post('relatorio/search/periodo', 'RelatorioController@search')->name('search.relatorio');