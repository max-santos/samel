# README #

Este README contem a informação necessária para instalar o projeto e configura-lo.
Este projeto foi desenvolvido utilizando Laravel e Postgresql. 
Também foi utilizado o template SM Admin [Ir para o site](https://startbootstrap.com/themes/sb-admin-2/)  
como base para o front-end, asim como classes
do Bootstrap [Ir para o site](https://getbootstrap.com/) e icones do site [fontawesome.com/](fontawesome.com/)
para que fosse possível desenvolver uma boa interface.


### Download do Projeto ###

* O projeto está no repositório abaixo neste link
* [Ir para o repositório](https://bitbucket.org/max-santos/samel/src/master/)

### Instalação do Projeto ###
Para instalar o projeto é ncessário que o computador possua os seguintes pacotes instalados

* Composer [Ir para o site](https://getcomposer.org/)
* Node JS [Ir para o site](https://nodejs.org/en/)
* Postgresql [Ir para o site](https://www.postgresql.org/)

### Banco de dados ###
Após a instalação do Postgresql, abra o PGAdmin ou outro SGBD que você utilize
e crie um banco vazio com o nome "samel".

Após criado o banco de dados vazio, vá ao arquivo .env do projeto e configure
a sua conexão com o banco de dados neste trecho do arquivo:

DB_USERNAME=postgres
DB_PASSWORD=0123456789

Você precisa alterar o usuário e a senha para o qual você utiliza no seu postgresql.
Após isso, o projeto estará configurado com o seu banco de dados.

### Instação do pacotes do Laravel ###
Após baixar o projeto no bitbucket, você precisa accesar a raiz do projeto 
laravel (no caso a ultima pasta samel).

Na raiz do projeto, abra um terminal e digite o comando:

composer install

Ele irá baixar todos os pacotes que o laravel utiliza com o composer. 
Após finalizado o download, vocẽ precisará baixar os pacotes do node, para isso você digita o seguinte comando no mesmo terminal:

npm install

Após finalizar o download dos pacotes, você precisa gerar os arquivos js para o front-end.
Neste caso, você irá digitar o comando: 

npm run dev

Após esse comando, você precisará criar as tabelas do banco de dados, já que você criou um banco de dados vazio no inicío 
dessa configuração. 

Para isso, no mesmo terminal, você precisará digitar o seguinte comando:

php artisan migrate

com este comando todas as tabelas serão criadas. Caso dê algum erro, provavelmente será por conta da configuração
da conexão com o banco de dados, para resolver isso verifique o passo acima de como configurar usuário e senha
do banco de dados no arquivo .env e insira as credenciais certas do seu banco de dados.


Após a finalização do comando você poderá iniciar o servidor para abrir a sua aplicação, use o comando:

php artisan run serve

Atenção: nao pare este comando, pois que ele deixa o servidor ativo executando sua aplicação. 
Verifique o link que o terminal irá informa para poder acessar sua aplicação em um navegador.

Dessa forma você irá acessar a pagina do sistema e para isso comece se cadastrando no sistema, pois foi criado também
o modulo de login para este teste.

Após criar um registro de usuário, você poderá acessaras paginas onde estao o modulo de médico, paciente, atendimento e relatorio.

Muito Obrigado pela paciência e pelo seu tempo ^^ 

